<%-- 
    Document   : Abonents
    Created on : Jul 4, 2016, 4:34:41 PM
    Author     : blast
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Список абонентов</h1>
        <table style="align-content: center" border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>ФИО</th>
                    <th>Адрес</th>
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${abonents}" var="abonent">
                    <tr>
                <form style="text-align:center;" method="GET" action="AddAbonent"  accept-charset="UTF-8">
                    <td>${abonent.getAbonentId()}</td>
                    <td>${abonent.getFullName()}</td>
                    <td>${abonent.getAddress()}</td>
                    <td><a href="./EditAbonent?abonentId=${abonent.getAbonentId()}">Изменить</a></td>
                    <td><a href="./DelAbonent?abonentId=${abonent.getAbonentId()}">Удалить</a></td>
                </form>
            </tr>
        </c:forEach>
    </tbody>
</table>

</body>
</html>
