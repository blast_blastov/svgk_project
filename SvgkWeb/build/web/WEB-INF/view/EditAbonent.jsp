<%-- 
    Document   : EditAbonent
    Created on : Jul 5, 2016, 11:30:06 PM
    Author     : blast
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Редактирование абонента</h1>
        <form method="POST" action="EditAbonent"  accept-charset="UTF-8">
            <input type="text" name="abonentId" value="${abonent.getAbonentId()}">
            Full name: <input type="text" name="fullName" value="${abonent.getFullName()}">
            Address: <input type="text" name="address" value="${abonent.getAddress()}"/>
            <input type="submit" value="save"/>
        </form>
    </body>
</html>
