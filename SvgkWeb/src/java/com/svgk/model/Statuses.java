package com.svgk.model;
// Generated Jul 5, 2016 6:14:15 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Statuses generated by hbm2java
 */
@Entity
@Table(name = "statuses", catalog = "svgk"
)
public class Statuses implements java.io.Serializable {

    private Integer statusId;
    private String name;
    private Set<Agreements> agreementses = new HashSet<Agreements>(0);

    public Statuses() {
    }

    public Statuses(String name) {
        this.name = name;
    }

    public Statuses(String name, Set<Agreements> agreementses) {
        this.name = name;
        this.agreementses = agreementses;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "status_id", unique = true, nullable = false)
    public Integer getStatusId() {
        return this.statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "statuses")
    public Set<Agreements> getAgreementses() {
        return this.agreementses;
    }

    public void setAgreementses(Set<Agreements> agreementses) {
        this.agreementses = agreementses;
    }

}
