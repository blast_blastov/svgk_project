/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.svgk.controller;

import com.svgk.businesslogic.AbonentsService;
import com.svgk.model.Abonents;
import com.svgk.util.Encoding;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author blast
 */
@WebServlet(name = "EditAbonent", urlPatterns = {"/EditAbonent"})
public class EditAbonent extends HttpServlet {

    @EJB
    private AbonentsService as;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/EditAbonent.jsp");
        Integer abonentId;
        Abonents abonent;
        if (request.getParameter("abonentId") != null) {
            abonentId = Integer.parseInt(request.getParameter("abonentId"));
            abonent = as.getAbonentByID(abonentId);
            request.setAttribute("abonent", abonent);
        }
        
        Abonents a=(Abonents)request.getAttribute("abonent");
        System.out.println(a.getAbonentId());
        System.out.println(a.getFullName());
        System.out.println(a.getAddress());
        
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();

        String fullName = Encoding.encodeToUTF8(request.getParameter("fullName"));
        String address = Encoding.encodeToUTF8(request.getParameter("address"));

        request.setAttribute("errors", false);

        if (fullName.length() == 0) {
            System.out.println("Empty name");
            request.setAttribute("errors", true);
            request.setAttribute("name_error", true);
            request.setAttribute("name", "");
        } else {
            request.setAttribute("name", fullName);
        }

        if (address.length() == 0) {
            System.out.println("Empty address");
            request.setAttribute("errors", true);
            request.setAttribute("address_error", true);
            request.setAttribute("address", "");
        } else {
            request.setAttribute("address", address);
        }

        if ((Boolean) request.getAttribute("errors")) {
            RequestDispatcher view = request.getRequestDispatcher("WEB-INF/view/EditAbonent.jsp");
            view.forward(request, response);
            return;
        }

        System.out.println(request.getParameter("abonentId"));
        
        Abonents abonent = new Abonents(fullName, address);
        abonent.setAbonentId(Integer.parseInt(request.getParameter("abonentId")));
        abonent.setFullName(fullName);
        abonent.setAddress(address);

        System.out.println(abonent.getAbonentId() + "\t" + request.getAttribute("abonent"));

        as.updateAbonent(abonent);

        response.sendRedirect("/SvgkWeb/AbonentsList");
        out.println("Done");
    }
}
