/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.svgk.util;

import java.nio.charset.Charset;

/**
 *
 * @author blast
 */
public class Encoding {

    public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    public static final Charset UTF_8 = Charset.forName("UTF-8");

    public static String encodeToUTF8(String input) {
        byte ptext[] = input.getBytes(ISO_8859_1);
        return new String(ptext, UTF_8);
    }

}
