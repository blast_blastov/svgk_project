/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.svgk.businesslogic;

import com.svgk.model.Abonents;
import com.svgk.util.HibernateUtil;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.hibernate.Session;

/**
 *
 * @author blast
 */
@Stateless
@LocalBean
public class AbonentsService {

    public List<Abonents> getList() {
        Session session = HibernateUtil.openSession();
        List<Abonents> aList = session.createQuery("from Abonents").list();

//        List<Abonents> aList = session.getNamedQuery("Abonents.findAll").list(); сделал сам
        session.close();
        return aList;
    }

    public void addAbonent(Abonents abonent) {
        Session session = HibernateUtil.openSession();
        session.getTransaction().begin();
        session.save(abonent);
        session.getTransaction().commit();
        session.close();
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public Abonents getAbonentByID(int abonentId) {
        Session session = HibernateUtil.openSession();

//        Abonents abonent = (Abonents) session.createQuery("from abonents where abonent_id = '" + abonentId + "'").list().get(0);
        Abonents abonent = (Abonents) session.load(Abonents.class, abonentId);
        return abonent;
    }

    public void updateAbonent(Abonents abonent) {
        Session session = HibernateUtil.openSession();
//        session.createQuery("from Abonents")
        session.getTransaction().begin();

        /* Query query = session.createQuery("update Abonent set fullName= :fullName where abonentId= :abonentId");
        query.setParameter("fullName", abonent.getFullName());
        query.setLong("abonentId", abonent.getAbonentId());
        int result = query.executeUpdate();
        System.out.println("Employee Update Status=" + result);*/
        session.update(abonent);

        session.getTransaction().commit();

        session.close();
    }

    public boolean deleteAbonent(int abonentId) {
        Session session = HibernateUtil.openSession();

        session.getTransaction().begin();

//        Abonents abonents = getAbonentByID(abonentId);
//        session.delete(abonents);
//        session.createQuery("delete from Abonents a where a.abonent_id = '" + abonentId + "'").executeUpdate();
        session.createSQLQuery("DELETE FROM abonents WHERE abonent_id=" + abonentId).executeUpdate();
        session.getTransaction().commit();

        session.close();
            
        
        return true;
    }
}
